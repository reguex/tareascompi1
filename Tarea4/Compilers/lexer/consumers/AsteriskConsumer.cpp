//
// Created by Edwin Herrera on 11/16/17.
//

#include "AsteriskConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::AsteriskConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::asterisk) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::AsteriskConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::AsteriskConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::OP_MULT;
    }

    raiseUndefinedTokenTypeForState(state);
}
