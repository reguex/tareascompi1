//
// Created by Edwin Herrera on 11/16/17.
//

#include "EqualsConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::EqualsConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::equal) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::equal) {
            return 3;
        }
    }

    return -1;
}

bool lexer::consumers::EqualsConsumer::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::consumers::EqualsConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::ASSIGNMENT;
    } else if (state == 3) {
        return lexer::TokenType::EQUALS;
    }

    raiseUndefinedTokenTypeForState(state);
}
