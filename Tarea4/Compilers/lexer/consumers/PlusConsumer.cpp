//
// Created by Edwin Herrera on 11/16/17.
//

#include "PlusConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::PlusConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::plus) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::PlusConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::PlusConsumer::getStateTokenType(int state, const std::string &lexeme) {
   if (state == 2) {
       return lexer::TokenType::OP_PLUS;
   }

    raiseUndefinedTokenTypeForState(state);
}
