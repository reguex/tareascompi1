//
// Created by Edwin Herrera on 11/16/17.
//

#include <iostream>
#include "Parser.h"
#include "../lexer/TokenType.h"

parser::Parser::Parser(lexer::Lexer *lexer) : lexer(lexer) {}

parser::Parser::~Parser() {
    delete currentToken;
}

void parser::Parser::parse() {
    currentToken = lexer->getNextToken();
    assignmentList();
    if (currentToken->getTokenType() != lexer::TokenType::END_OF_FILE) {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {"EOF"});
    }
}

void parser::Parser::raiseUnexpectedTokenError(std::string found, std::initializer_list<std::string> expected) {
    std::string expectedStr;
    for (const auto &e : expected) {
        if (!expectedStr.empty()) {
            expectedStr += ", ";
        }
        expectedStr += e;
    }
    std::string error = "Unexpected token. Expected " + expectedStr + " Found: " + found;
    throw std::invalid_argument(error);
}

void parser::Parser::consumeToken() {
    if (currentToken != nullptr) {
        std::cout << *currentToken << std::endl;
    }
    delete currentToken;
    currentToken = lexer->getNextToken();
}

void parser::Parser::E() {
    T();
    Ep();
}

void parser::Parser::Ep() {
    if (currentToken->tokenType == lexer::TokenType::OP_PLUS) {
        consumeToken();
        T();
        Ep();
    } else if (currentToken->tokenType == lexer::TokenType::OP_SUB) {
        consumeToken();
        T();
        Ep();
    } else {
        return;
    }
}

void parser::Parser::T() {
    F();
    Tp();
}

void parser::Parser::Tp() {
    if (currentToken->tokenType == lexer::TokenType::OP_MULT) {
        consumeToken();
        F();
        Tp();
    } else if (currentToken->tokenType == lexer::TokenType::OP_DIV) {
        consumeToken();
        F();
        Tp();
    } else {
        return;
    }
}

void parser::Parser::F() {
    if (currentToken->tokenType == lexer::TokenType::NUMBER || currentToken->tokenType == lexer::TokenType::ID) {
        consumeToken();
    } else if (currentToken->tokenType == lexer::TokenType::LEFT_PAREN) {
        consumeToken();
        E();
        if (currentToken->tokenType == lexer::TokenType::RIGHT_PAREN) {
            consumeToken();
        } else {
            raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::RIGHT_PAREN});
        }
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {
                lexer::TokenType::LEFT_PAREN, lexer::TokenType::ID, lexer::TokenType::NUMBER
        });
    }
}

void parser::Parser::assignment() {
    if (currentToken->tokenType == lexer::TokenType::ID) {
        consumeToken();
        if (currentToken->getTokenType() == lexer::TokenType::ASSIGNMENT) {
            consumeToken();
            E();
        } else {
            raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::ASSIGNMENT});
        }
    } else {
        return;
    }
}

void parser::Parser::assignmentList() {
    assignment();
    if (currentToken->getTokenType() == lexer::TokenType::SEMICOLON) {
        consumeToken();
        assignment();
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::SEMICOLON});
    }
}
