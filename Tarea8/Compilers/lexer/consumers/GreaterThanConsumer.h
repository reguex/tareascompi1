//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_GREATERTHANCONSUMER_H
#define COMPILERS_GREATERTHANCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class GreaterThanConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state, const std::string &lexeme) override;
        };
    }
}


#endif //COMPILERS_GREATERTHANCONSUMER_H
