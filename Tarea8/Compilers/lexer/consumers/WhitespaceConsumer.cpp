//
// Created by Edwin Herrera on 11/16/17.
//

#include "WhitespaceConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::WhitespaceConsumer::delta(int current, int value) {
    if (current == 1) {
        if (util::Chars::isWhiteSpace(value)) {
            return 2;
        }
    } else if (current == 2) {
        if (util::Chars::isWhiteSpace(value)) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::WhitespaceConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::WhitespaceConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::WHITESPACE;
    }

    raiseUndefinedTokenTypeForState(state);
}
