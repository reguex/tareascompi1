//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_TOKENTYPE_H
#define COMPILERS_TOKENTYPE_H


#include <string>

namespace lexer {
    class TokenType {
    public:
        static const std::string ID;
        static const std::string WHITESPACE;
        static const std::string COMMENT;
        static const std::string OP_DIV;
        static const std::string OP_MULT;
        static const std::string OP_PLUS;
        static const std::string OP_SUB;
        static const std::string LEFT_PAREN;
        static const std::string RIGHT_PAREN;
        static const std::string NUMBER;
        static const std::string EQUALS;
        static const std::string ASSIGNMENT;
        static const std::string SEMICOLON;
        static const std::string END_OF_FILE;
        static const std::string LESS_THAN;
        static const std::string GREATER_THAN;
        static const std::string LESS_THAN_OR_EQUAL;
        static const std::string GREATER_THAN_OR_EQUAL;

        static const std::string KW_IF;
        static const std::string KW_END;
        static const std::string KW_THEN;
        static const std::string KW_PRINT;
    };
}


#endif //COMPILERS_TOKENTYPE_H
