//
// Created by Edwin Herrera on 11/29/17.
//

#include "BinaryExpressionNode.h"

ast::BinaryExpressionNode::BinaryExpressionNode(ast::Node *left, ast::Node *right) : left(left), right(right) {}

ast::BinaryExpressionNode::~BinaryExpressionNode() {
    delete left;
    delete right;
}

ast::Node *ast::BinaryExpressionNode::getLeft() const {
    return left;
}

ast::Node *ast::BinaryExpressionNode::getRight() const {
    return right;
}
