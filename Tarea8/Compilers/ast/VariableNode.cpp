//
// Created by Edwin Herrera on 11/29/17.
//

#include "VariableNode.h"

ast::VariableNode::VariableNode(const std::string &varName) : varName(varName) {}

const std::string &ast::VariableNode::getVarName() const {
    return varName;
}

std::string ast::VariableNode::toString() {
    return "VariableNode( " + varName + " )";
}


