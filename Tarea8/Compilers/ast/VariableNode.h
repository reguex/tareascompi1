//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_VARIABLENODE_H
#define COMPILERS_VARIABLENODE_H


#include <string>
#include "Node.h"

namespace ast {
    class VariableNode : public Node {
        std::string varName;
    public:
        explicit VariableNode(const std::string &varName);

        const std::string &getVarName() const;

        std::string toString() override;
    };
}


#endif //COMPILERS_VARIABLENODE_H
