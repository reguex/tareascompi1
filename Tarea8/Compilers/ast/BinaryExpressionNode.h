//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_BINARYNODE_H
#define COMPILERS_BINARYNODE_H


#include "Node.h"

namespace ast {
    class BinaryExpressionNode : public Node {
        Node *left;
        Node *right;

    public:
        BinaryExpressionNode(Node *left, Node *right);

        virtual ~BinaryExpressionNode();

        Node *getLeft() const;

        Node *getRight() const;
    };
}


#endif //COMPILERS_BINARYNODE_H
