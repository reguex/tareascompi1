//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_DIVISIONEXPRESSIONNODE_H
#define COMPILERS_DIVISIONEXPRESSIONNODE_H


#include "BinaryExpressionNode.h"

namespace ast {
    class DivisionExpressionNode : public BinaryExpressionNode {
    public:
        DivisionExpressionNode(Node *left, Node *right);

        std::string toString() override;

    };
}


#endif //COMPILERS_DIVISIONEXPRESSIONNODE_H
