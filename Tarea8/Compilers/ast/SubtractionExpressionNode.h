//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_SUBTRACTIONEXPRESSIONNODE_H
#define COMPILERS_SUBTRACTIONEXPRESSIONNODE_H


#include "BinaryExpressionNode.h"

namespace ast {
    class SubtractionExpressionNode : public BinaryExpressionNode {
    public:
        SubtractionExpressionNode(Node *left, Node *right);

        std::string toString() override;
    };
}


#endif //COMPILERS_SUBTRACTIONEXPRESSIONNODE_H
