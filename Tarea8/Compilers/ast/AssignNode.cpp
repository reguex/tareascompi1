//
// Created by Edwin Herrera on 11/29/17.
//

#include "AssignNode.h"

ast::AssignNode::AssignNode(ast::Node *left, ast::Node *right)
        : BinaryExpressionNode(left, right) {}

std::string ast::AssignNode::toString() {
    return "AssignNode( left: " + getLeft()->toString() + ", right: " + getRight()->toString() + " )";
}
