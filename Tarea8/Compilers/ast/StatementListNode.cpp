//
// Created by Edwin Herrera on 11/29/17.
//

#include "StatementListNode.h"

ast::StatementListNode::StatementListNode(std::vector<ast::Node *> *children) : children(children) {}

ast::StatementListNode::~StatementListNode() {
    for (auto child : *children) {
        delete child;
    }

    delete children;
}

std::string ast::StatementListNode::toString() {
    std::string childrenStr;
    for (auto child : *children) {
        if (!childrenStr.empty()) {
            childrenStr += ", ";
        }
        childrenStr += child->toString();
    }

    return "StatementListNode( " + childrenStr + " )";
}
