//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_MULTIPLICATIONEXPRESSIONNODE_H
#define COMPILERS_MULTIPLICATIONEXPRESSIONNODE_H


#include "BinaryExpressionNode.h"

namespace ast {
    class MultiplicationExpressionNode : public BinaryExpressionNode {
    public:
        MultiplicationExpressionNode(Node *left, Node *right);

        std::string toString() override;

    };
}


#endif //COMPILERS_MULTIPLICATIONEXPRESSIONNODE_H
