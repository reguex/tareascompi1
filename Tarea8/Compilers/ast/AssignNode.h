//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_ASSIGNNODE_H
#define COMPILERS_ASSIGNNODE_H


#include "Node.h"
#include "BinaryExpressionNode.h"

namespace ast {
    class AssignNode : public BinaryExpressionNode {
    public:
        AssignNode(ast::Node *left, ast::Node *right);

        std::string toString() override;
    };
}


#endif //COMPILERS_ASSIGNNODE_H
