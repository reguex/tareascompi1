//
// Created by Edwin Herrera on 11/16/17.
//

#include <iostream>
#include <vector>
#include "Parser.h"
#include "../lexer/TokenType.h"
#include "../ast/StatementListNode.h"
#include "../ast/NumNode.h"
#include "../ast/VariableNode.h"
#include "../ast/MultiplicationExpressionNode.h"
#include "../ast/DivisionExpressionNode.h"
#include "../ast/SubtractionExpressionNode.h"
#include "../ast/AdditionExpressionNode.h"
#include "../ast/AssignNode.h"

parser::Parser::Parser(lexer::Lexer *lexer) : lexer(lexer) {
}

parser::Parser::~Parser() {
    delete currentToken;
}

ast::Node *parser::Parser::parse() {
    currentToken = lexer->getNextToken();

    auto *statements = new std::vector<ast::Node *>();
    while (currentToken->getTokenType() != lexer::TokenType::END_OF_FILE) {
        auto statement = assignment();
        consumeToken({lexer::TokenType::SEMICOLON});
        statements->push_back(statement);
    }

    return new ast::StatementListNode(statements);
}

void parser::Parser::raiseUnexpectedTokenError(std::initializer_list<std::string> expected) {
    std::string expectedStr;
    for (const auto &e : expected) {
        if (!expectedStr.empty()) {
            expectedStr += ", ";
        }
        expectedStr += e;
    }
    std::string error = "Unexpected token. Expected " + expectedStr + " Found: " + currentToken->getTokenType();
    throw std::invalid_argument(error);
}

void parser::Parser::consumeToken(std::initializer_list<std::string> types) {
    if (currentToken != nullptr) {
        std::cout << *currentToken << std::endl;
    }

    for (const auto &type : types) {
        if (currentToken->getTokenType() == type) {
            delete currentToken;
            currentToken = lexer->getNextToken();
            return;
        }
    }

    raiseUnexpectedTokenError(types);
}


ast::Node *parser::Parser::assignment() {
    std::string varName = currentToken->getLexeme();
    consumeToken({lexer::TokenType::ID});

    consumeToken({lexer::TokenType::ASSIGNMENT});

    auto result = expression();

    return new ast::AssignNode(new ast::VariableNode(varName), result);
}

ast::Node *parser::Parser::expression() {
    auto result = term();
    while (currentToken->getTokenType() == lexer::TokenType::OP_SUB ||
           currentToken->getTokenType() == lexer::TokenType::OP_PLUS) {

        std::string op = currentToken->getTokenType();
        if (op == lexer::TokenType::OP_SUB) {
            consumeToken({lexer::TokenType::OP_SUB});
            result = new ast::SubtractionExpressionNode(result, term());
        } else if (op == lexer::TokenType::OP_PLUS) {
            consumeToken({lexer::TokenType::OP_PLUS});
            result = new ast::AdditionExpressionNode(result, term());
        }
    }
    return result;
}

ast::Node *parser::Parser::term() {
    ast::Node* result = factor();
    while (currentToken->getTokenType() == lexer::TokenType::OP_MULT ||
           currentToken->getTokenType() == lexer::TokenType::OP_DIV) {

        std::string op = currentToken->getTokenType();
        if (op == lexer::TokenType::OP_MULT) {
            consumeToken({lexer::TokenType::OP_MULT});
            result = new ast::MultiplicationExpressionNode(result, term());
        } else if (op == lexer::TokenType::OP_DIV) {
            consumeToken({lexer::TokenType::OP_DIV});
            result = new ast::DivisionExpressionNode(result, term());
        }
    }

    return result;
}

ast::Node *parser::Parser::factor() {
    std::string value = currentToken->getLexeme();

    ast::Node *result;
    if (currentToken->getTokenType() == lexer::TokenType::NUMBER) {
        consumeToken({lexer::TokenType::NUMBER});
        result = new ast::NumNode(std::atoi(value.c_str())); // NOLINT
    } else if (currentToken->getTokenType() == lexer::TokenType::ID) {
        consumeToken({lexer::TokenType::ID});
        result = new ast::VariableNode(value);
    } else if (currentToken->getTokenType() == lexer::TokenType::LEFT_PAREN) {
        consumeToken({lexer::TokenType::LEFT_PAREN});
        result = expression();
        consumeToken({lexer::TokenType::RIGHT_PAREN});
    } else {
        raiseUnexpectedTokenError({lexer::TokenType::NUMBER, lexer::TokenType::ID});
    }

    return result;
}
