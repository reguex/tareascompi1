//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_PARSER_H
#define COMPILERS_PARSER_H


#include <string>
#include "../lexer/Lexer.h"
#include "../ast/Node.h"
#include <initializer_list>

namespace parser {
    class Parser {
        lexer::Lexer *lexer;
        lexer::Token *currentToken;

    public:
        explicit Parser(lexer::Lexer *lexer);

        virtual ~Parser();

        ast::Node *parse();

    private:
        ast::Node* assignment();
        ast::Node* expression();
        ast::Node* term();
        ast::Node* factor();

    private:
        [[noreturn]] void raiseUnexpectedTokenError(std::initializer_list<std::string> expected);

        void consumeToken(std::initializer_list<std::string> types);
    };
}


#endif //COMPILERS_PARSER_H
