//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_CHARS_H
#define COMPILERS_CHARS_H


#include <cctype>

namespace util {
    class Chars {
    public:
        static const int zero = 48;
        static const int one = 49;
        static const int X = 88;
        static const int x = 120;
        static const int b = 98;
        static const int B = 66;
        static const int h = 104;
        static const int H = 72;
        static const int underscore = 95;
        static const int plus = 43;
        static const int minus = 45;
        static const int slash = 47;
        static const int leftParentheses = 40;
        static const int rightParentheses = 41;
        static const int equal = 61;
        static const int asterisk = 42;
        static const int hash = 35;
        static const int lessThan = 60;
        static const int greaterThan = 62;
        static const int semicolon = 59;

        static const bool isBinaryNumber(int c) {
            return c == zero || c == one;
        }

        static const bool isWhiteSpace(int c) {
            auto c_char = static_cast<char>(c);
            return c_char == ' ' || c_char == '\n' || c_char == '\t';
        }

        static const bool isLetter(int c) {
            return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
        }

        static const bool isNumber(int c) {
            return isdigit(c);
        }

        static const bool isHexNumber(int c) {
            return ishexnumber(c);
        }
    };
}


#endif //COMPILERS_CHARS_H
