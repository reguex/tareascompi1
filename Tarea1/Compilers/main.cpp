#include <iostream>
#include <fstream>
#include "lexer/Lexer.h"

int main() {
    std::string file;
    std::cout << "File: ";
    std::cin >> file;

    std::ifstream ifs;

    ifs.open(file);
    lexer::Lexer lexer(ifs);
    lexer::Token *token;

    do {
        delete token;
        token = lexer.getNextToken();
        if (token != nullptr) {
            std::cout << *token << std::endl;
        }
    } while (token != nullptr);

    ifs.close();
}