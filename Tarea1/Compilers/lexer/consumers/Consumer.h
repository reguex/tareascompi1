//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_CONSUMER_H
#define COMPILERS_CONSUMER_H


#include <string>
#include <fstream>
#include "../Token.h"

namespace lexer {
    namespace consumers {

        class Consumer {
            int initial;
        public:
            explicit Consumer(int initial);
            Consumer();

            Token *process(std::ifstream &source);

        protected:
            virtual int delta(int current, int value) = 0;
            virtual bool isFinal(int state) = 0;
            virtual std::string getStateTokenType(int state) = 0;

        public:
            virtual ~Consumer();

        protected:
            [[noreturn]] void raiseUndefinedTokenTypeForState(int state);
        };

    }
}


#endif //COMPILERS_CONSUMER_H
