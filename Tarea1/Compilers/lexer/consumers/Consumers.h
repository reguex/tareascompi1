//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_CONSUMERS_H
#define COMPILERS_CONSUMERS_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {

        /**
         * Consumer Factory
         */
        class Consumers {
        public:
            static Consumer *getAppropriateConsumer(int first);
        };
    }
}


#endif //COMPILERS_CONSUMERS_H
