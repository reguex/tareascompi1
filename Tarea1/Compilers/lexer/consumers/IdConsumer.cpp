//
// Created by Edwin Herrera on 11/16/17.
//

#include "IdConsumer.h"
#include "../TokenType.h"

lexer::consumers::IdConsumer::IdConsumer() : Consumer() {}

int lexer::consumers::IdConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::underscore || util::Chars::isLetter(value)) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::underscore || util::Chars::isNumber(value) || util::Chars::isLetter(value)) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::IdConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::IdConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return lexer::TokenType::ID;
    }

    raiseUndefinedTokenTypeForState(state);
}
