//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_NODE_H
#define COMPILERS_NODE_H


#include <string>

namespace ast {
    class Node {
    public:
        virtual std::string toString() = 0;
    };
}


#endif //COMPILERS_NODE_H
