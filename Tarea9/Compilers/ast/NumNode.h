//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_NUMNODE_H
#define COMPILERS_NUMNODE_H


#include "Node.h"

namespace ast {
    class NumNode : public Node {
        int value;
    public:
        explicit NumNode(int value);

        int getValue() const;

        std::string toString() override;
    };
}


#endif //COMPILERS_NUMNODE_H
