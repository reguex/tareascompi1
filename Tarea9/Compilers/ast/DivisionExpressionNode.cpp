//
// Created by Edwin Herrera on 11/29/17.
//

#include "DivisionExpressionNode.h"

ast::DivisionExpressionNode::DivisionExpressionNode(ast::Node *left, ast::Node *right) :
        BinaryExpressionNode(left, right) {}

std::string ast::DivisionExpressionNode::toString() {
    return "DivisionExpressionNode( left: " + getLeft()->toString() + ", right: " + getRight()->toString() + " )";
}
