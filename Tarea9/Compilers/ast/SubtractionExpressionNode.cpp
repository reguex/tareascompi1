//
// Created by Edwin Herrera on 11/29/17.
//

#include "SubtractionExpressionNode.h"

ast::SubtractionExpressionNode::SubtractionExpressionNode(ast::Node *left, ast::Node *right) :
        BinaryExpressionNode(left, right) {}

std::string ast::SubtractionExpressionNode::toString() {
    return "SubtractionExpressionNode( left: " + getLeft()->toString() + ", right: " + getRight()->toString() + " )";
}
