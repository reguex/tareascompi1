//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_STATEMENTLISTNODE_H
#define COMPILERS_STATEMENTLISTNODE_H


#include <vector>
#include "Node.h"

namespace ast {
    class StatementListNode : public Node {
        std::vector<Node *> *children;
    public:
        StatementListNode(std::vector<Node *> *children);

        virtual ~StatementListNode();

        std::string toString() override;

    };
}


#endif //COMPILERS_STATEMENTLISTNODE_H
