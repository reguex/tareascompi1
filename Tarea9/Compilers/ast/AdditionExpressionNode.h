//
// Created by Edwin Herrera on 11/29/17.
//

#ifndef COMPILERS_ADDITIONEXPRESSIONNODE_H
#define COMPILERS_ADDITIONEXPRESSIONNODE_H


#include "BinaryExpressionNode.h"

namespace ast {
    class AdditionExpressionNode : public BinaryExpressionNode {
    public:
        AdditionExpressionNode(Node *left, Node *right);

        std::string toString() override;

    };

}

#endif //COMPILERS_ADDITIONEXPRESSIONNODE_H
