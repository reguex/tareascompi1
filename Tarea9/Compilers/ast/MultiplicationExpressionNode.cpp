//
// Created by Edwin Herrera on 11/29/17.
//

#include "MultiplicationExpressionNode.h"

ast::MultiplicationExpressionNode::MultiplicationExpressionNode(ast::Node *left, ast::Node *right)
        : BinaryExpressionNode(left, right) {}

std::string ast::MultiplicationExpressionNode::toString() {
    return "MultiplicationExpressionNode( left: " + getLeft()->toString() + ", right: " + getRight()->toString() + " )";
}
