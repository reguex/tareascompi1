//
// Created by Edwin Herrera on 11/29/17.
//

#include "NumNode.h"

ast::NumNode::NumNode(int value) : value(value) {}

int ast::NumNode::getValue() const {
    return value;
}

std::string ast::NumNode::toString() {
    return "NumNode( " + std::to_string(value) + " )";
}
