//
// Created by Edwin Herrera on 11/29/17.
//

#include "AdditionExpressionNode.h"

ast::AdditionExpressionNode::AdditionExpressionNode(ast::Node *left, ast::Node *right) :
        BinaryExpressionNode(left, right) {}

std::string ast::AdditionExpressionNode::toString() {
    return "AdditionExpressionNode( left: " + getLeft()->toString() + ", right: " + getRight()->toString() + " )";
}
