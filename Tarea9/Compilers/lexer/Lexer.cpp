//
// Created by Edwin Herrera on 11/16/17.
//

#include <iostream>
#include "Lexer.h"
#include "TokenType.h"
#include "consumers/Consumers.h"

lexer::Lexer::Lexer(std::ifstream &input) : input(input) {}

lexer::Token *lexer::Lexer::getNextToken() {
    if (input.eof() || input.peek() == -1) {
        return new Token("", lexer::TokenType::END_OF_FILE);
    }

    consumers::Consumer *consumer = consumers::Consumers::getAppropriateConsumer(input.peek());
    if (consumer == nullptr) {
        std::string error = "Invalid Character '";
        error += static_cast<char>(input.peek());
        error += "'";
        throw std::invalid_argument(error);
    }

    Token *token = consumer->process(input);
    if (token->getTokenType() == lexer::TokenType::WHITESPACE || token->getTokenType() == lexer::TokenType::COMMENT) {
        delete token;
        delete consumer;
        return getNextToken();
    }

    return token;
}
