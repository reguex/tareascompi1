//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_TOKEN_H
#define COMPILERS_TOKEN_H

#include <string>
#include <ostream>

namespace lexer {
    struct Token {
        const std::string lexeme;
        const std::string tokenType;

        Token(const std::string &lexeme, const std::string &tokenType) : lexeme(lexeme), tokenType(tokenType) {}

        const std::string &getLexeme() const {
            return lexeme;
        }

        const std::string &getTokenType() const {
            return tokenType;
        }

        friend std::ostream &operator<<(std::ostream &os, const Token &token) {
            os << "lexeme: " << token.lexeme << " tokenType: " << token.tokenType;
            return os;
        }
    };
}


#endif //COMPILERS_TOKEN_H
