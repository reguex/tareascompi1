//
// Created by Edwin Herrera on 11/16/17.
//

#include "TokenType.h"


const std::string lexer::TokenType::ID = "ID"; // NOLINT
const std::string lexer::TokenType::WHITESPACE = "WHITESPACE"; // NOLINT
const std::string lexer::TokenType::COMMENT = "COMMENT"; // NOLINT
const std::string lexer::TokenType::OP_DIV = "DIV"; // NOLINT
const std::string lexer::TokenType::OP_MULT = "MULT"; // NOLINT
const std::string lexer::TokenType::OP_PLUS = "PLUS"; // NOLINT
const std::string lexer::TokenType::OP_SUB = "SUB"; // NOLINT
const std::string lexer::TokenType::LEFT_PAREN = "LEFT PARENTHESES"; // NOLINT
const std::string lexer::TokenType::RIGHT_PAREN = "RIGHT PARENTHESES"; // NOLINT
const std::string lexer::TokenType::NUMBER = "NUMBER"; // NOLINT
const std::string lexer::TokenType::EQUALS = "EQUALS"; // NOLINT
const std::string lexer::TokenType::ASSIGNMENT = "ASSIGNMENT"; // NOLINT
const std::string lexer::TokenType::KW_IF = "IF"; // NOLINT
const std::string lexer::TokenType::KW_END = "END"; // NOLINT
const std::string lexer::TokenType::KW_THEN = "THEN"; // NOLINT
const std::string lexer::TokenType::SEMICOLON = "SEMICOLON"; // NOLINT
const std::string lexer::TokenType::END_OF_FILE = "EOF"; // NOLINT
const std::string lexer::TokenType::KW_PRINT = "PRINT"; // NOLINT
const std::string lexer::TokenType::LESS_THAN = "LESS THAN"; // NOLINT
const std::string lexer::TokenType::GREATER_THAN = "GREATER THAN"; // NOLINT
const std::string lexer::TokenType::LESS_THAN_OR_EQUAL = "LESS THAN OR EQUAL"; // NOLINT
const std::string lexer::TokenType::GREATER_THAN_OR_EQUAL = "GREATER THAN OR EQUAL"; // NOLINT