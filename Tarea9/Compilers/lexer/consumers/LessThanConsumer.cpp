//
// Created by Edwin Herrera on 11/16/17.
//

#include "LessThanConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::LessThanConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::lessThan) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::equal) {
            return 3;
        }
    }

    return -1;
}

bool lexer::consumers::LessThanConsumer::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::consumers::LessThanConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::LESS_THAN;
    } else if (state == 3) {
        return lexer::TokenType::LESS_THAN_OR_EQUAL;
    }

    raiseUndefinedTokenTypeForState(2);
}
