//
// Created by Edwin Herrera on 11/16/17.
//

#include "Consumers.h"
#include "../../util/Chars.h"
#include "IdConsumer.h"
#include "WhitespaceConsumer.h"
#include "SlashConsumer.h"
#include "AsteriskConsumer.h"
#include "PlusConsumer.h"
#include "MinusConsumer.h"
#include "LeftParenthesesConsumer.h"
#include "RightParenthesesConsumer.h"
#include "NumberConsumer.h"
#include "EqualsConsumer.h"
#include "SemicolonConsumer.h"
#include "GreaterThanConsumer.h"
#include "LessThanConsumer.h"

lexer::consumers::Consumer *lexer::consumers::Consumers::getAppropriateConsumer(int first) {
    if (util::Chars::isLetter(first) || first == util::Chars::underscore) {
        return new consumers::IdConsumer();
    } else if (util::Chars::isWhiteSpace(first)) {
        return new consumers::WhitespaceConsumer();
    } else if (first == util::Chars::slash) {
        return new consumers::SlashConsumer();
    } else if (first == util::Chars::asterisk) {
        return new consumers::AsteriskConsumer();
    } else if (first == util::Chars::plus) {
        return new consumers::PlusConsumer();
    } else if (first == util::Chars::minus) {
        return new consumers::MinusConsumer();
    } else if (first == util::Chars::leftParentheses) {
        return new consumers::LeftParenthesesConsumer();
    } else if (first == util::Chars::rightParentheses) {
        return new consumers::RightParenthesesConsumer();
    } else if (util::Chars::isNumber(first)) {
        return new consumers::NumberConsumer();
    } else if (first == util::Chars::equal) {
        return new consumers::EqualsConsumer();
    } else if (first == util::Chars::semicolon) {
        return new consumers::SemicolonConsumer();
    } else if (first == util::Chars::greaterThan) {
        return new consumers::GreaterThanConsumer();
    } else if (first == util::Chars::lessThan) {
        return new consumers::LessThanConsumer();
    }

    return nullptr;
}
