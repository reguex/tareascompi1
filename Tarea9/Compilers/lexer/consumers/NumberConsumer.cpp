//
// Created by Edwin Herrera on 11/16/17.
//

#include "NumberConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::NumberConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::zero) {
            return 2;
        } else if (util::Chars::isNumber(value)) {
            return 3;
        }
    } else if (current == 2) {
        if (value == util::Chars::b || value == util::Chars::B) {
            return 4;
        } else if (value == util::Chars::x || value == util::Chars::X) {
            return 5;
        }
    } else if (current == 3) {
        if (util::Chars::isNumber(value)) {
            return 3;
        }
    } else if (current == 4) {
        if (util::Chars::isBinaryNumber(value)) {
            return 6;
        }
    } else if (current == 5) {
        if (util::Chars::isHexNumber(value)) {
            return 7;
        }
    } else if (current == 6) {
        if (util::Chars::isBinaryNumber(value)) {
            return 6;
        }
    } else if (current == 7) {
        if (util::Chars::isHexNumber(value)) {
            return 7;
        }
    }

    return -1;
}

bool lexer::consumers::NumberConsumer::isFinal(int state) {
    return state == 2 ||
           state == 3 ||
           state == 6 ||
           state == 7;
}

std::string lexer::consumers::NumberConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2 ||
        state == 3 ||
        state == 6 ||
        state == 7) {

        return lexer::TokenType::NUMBER;
    }

    raiseUndefinedTokenTypeForState(state);
}
