//
// Created by Edwin Herrera on 11/16/17.
//

#include "GreaterThanConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::GreaterThanConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::greaterThan) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::equal) {
            return 3;
        }
    }

    return -1;
}

bool lexer::consumers::GreaterThanConsumer::isFinal(int state) {
    return state == 2 || state == 3;
}

std::string lexer::consumers::GreaterThanConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::GREATER_THAN;
    } else if (state == 3) {
        return lexer::TokenType::GREATER_THAN_OR_EQUAL;
    }

    raiseUndefinedTokenTypeForState(2);
}
