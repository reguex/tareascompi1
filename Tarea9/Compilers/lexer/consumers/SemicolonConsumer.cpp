//
// Created by Edwin Herrera on 11/16/17.
//

#include "SemicolonConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::SemicolonConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::semicolon) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::SemicolonConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::SemicolonConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::SEMICOLON;
    }

    raiseUndefinedTokenTypeForState(state);
}
