#include <iostream>
#include <fstream>
#include "lexer/Lexer.h"
#include "parser/Parser.h"

int main() {
    std::string file;
    std::cout << "File: ";
    std::cin >> file;

    std::ifstream ifs;

    ifs.open(file);
    lexer::Lexer lexer(ifs);
    parser::Parser parser(&lexer);

    parser.parse();

    ifs.close();
}