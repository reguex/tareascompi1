//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_PARSER_H
#define COMPILERS_PARSER_H


#include <string>
#include "../lexer/Lexer.h"
#include <initializer_list>

namespace parser {
    class Parser {
        lexer::Lexer *lexer;
        lexer::Token *currentToken;

    public:
        explicit Parser(lexer::Lexer *lexer);

        virtual ~Parser();

        void parse();

    private:
        void consumeToken();
        void assignmentList();
        void assignment();
        void E();
        void Ep();
        void T();
        void Tp();
        void F();
        void assign();

    private:
        [[noreturn]] void raiseUnexpectedTokenError(std::string found, std::initializer_list<std::string> expected);

        void topLevel();

        void printStatement();

        void comparison();
    };
}


#endif //COMPILERS_PARSER_H
