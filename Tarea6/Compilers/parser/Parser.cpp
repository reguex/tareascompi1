//
// Created by Edwin Herrera on 11/16/17.
//

#include <iostream>
#include "Parser.h"
#include "../lexer/TokenType.h"

parser::Parser::Parser(lexer::Lexer *lexer) : lexer(lexer) {}

parser::Parser::~Parser() {
    delete currentToken;
}

void parser::Parser::parse() {
    currentToken = lexer->getNextToken();
    topLevel();
    if (currentToken->getTokenType() != lexer::TokenType::END_OF_FILE) {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {"EOF"});
    }
}

void parser::Parser::raiseUnexpectedTokenError(std::string found, std::initializer_list<std::string> expected) {
    std::string expectedStr;
    for (const auto &e : expected) {
        if (!expectedStr.empty()) {
            expectedStr += ", ";
        }
        expectedStr += e;
    }
    std::string error = "Unexpected token. Expected " + expectedStr + " Found: " + found;
    throw std::invalid_argument(error);
}

void parser::Parser::consumeToken() {
    if (currentToken != nullptr) {
        std::cout << *currentToken << std::endl;
    }
    delete currentToken;
    currentToken = lexer->getNextToken();
}

void parser::Parser::E() {
    T();
    while (currentToken->getTokenType() == lexer::TokenType::OP_PLUS ||
           currentToken->getTokenType() == lexer::TokenType::OP_SUB) {

        consumeToken();
        T();
    }
}

void parser::Parser::T() {
    F();
    while (currentToken->getTokenType() == lexer::TokenType::OP_MULT ||
           currentToken->getTokenType() == lexer::TokenType::OP_DIV) {

        consumeToken();
        F();
    }
}

void parser::Parser::F() {
    if (currentToken->getTokenType() == lexer::TokenType::LEFT_PAREN) {
        consumeToken();
        E();
        if (currentToken->getTokenType() == lexer::TokenType::RIGHT_PAREN) {
            consumeToken();
        } else {
            raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::RIGHT_PAREN});
        }
    } else if (currentToken->getTokenType() == lexer::TokenType::ID ||
               currentToken->getTokenType() == lexer::TokenType::NUMBER) {
        consumeToken();
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {
                lexer::TokenType::LEFT_PAREN, lexer::TokenType::ID, lexer::TokenType::NUMBER
        });
    }
}

void parser::Parser::assign() {
    if (currentToken->getTokenType() == lexer::TokenType::ID) {
        consumeToken();
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::ID});
    }

    if (currentToken->getTokenType() == lexer::TokenType::ASSIGNMENT) {
        consumeToken();
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::ASSIGNMENT});
    }

    comparison();
}

void parser::Parser::topLevel() {
    if (currentToken->getTokenType() == lexer::TokenType::KW_PRINT) {
        printStatement();
        if (currentToken->getTokenType() == lexer::TokenType::SEMICOLON) {
            consumeToken();
        } else {
            raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::SEMICOLON});
        }
    } else if (currentToken->getTokenType() == lexer::TokenType::ID) {
        assign();
        if (currentToken->getTokenType() == lexer::TokenType::SEMICOLON) {
            consumeToken();
        } else {
            raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::SEMICOLON});
        }
    }

    while (currentToken->getTokenType() == lexer::TokenType::KW_PRINT ||
           currentToken->getTokenType() == lexer::TokenType::ID) {

        topLevel();
    }
}

void parser::Parser::printStatement() {
    if (currentToken->getTokenType() == lexer::TokenType::KW_PRINT) {
        consumeToken();
    } else {
        raiseUnexpectedTokenError(currentToken->getTokenType(), {lexer::TokenType::ID});
    }
    comparison();
}

void parser::Parser::comparison() {
    E();
    if (currentToken->getTokenType() == lexer::TokenType::LESS_THAN ||
        currentToken->getTokenType() == lexer::TokenType::LESS_THAN_OR_EQUAL) {

        consumeToken();
        E();
    } else if (currentToken->getTokenType() == lexer::TokenType::GREATER_THAN ||
               currentToken->getTokenType() == lexer::TokenType::GREATER_THAN_OR_EQUAL) {

        consumeToken();
        E();
    } else {
        return;
    }
}