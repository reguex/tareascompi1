//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_ASTERISKCONSUMER_H
#define COMPILERS_ASTERISKCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class AsteriskConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state, const std::string &lexeme) override;
        };
    }
}


#endif //COMPILERS_ASTERISKCONSUMER_H
