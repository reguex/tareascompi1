//
// Created by Edwin Herrera on 11/16/17.
//

#include "LeftParenthesesConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::LeftParenthesesConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::leftParentheses) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::LeftParenthesesConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::LeftParenthesesConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::LEFT_PAREN;
    }

    raiseUndefinedTokenTypeForState(state);
}
