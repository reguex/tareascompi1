//
// Created by Edwin Herrera on 11/16/17.
//

#include "Consumer.h"


lexer::consumers::Consumer::Consumer(int initial) : initial(initial) {}

lexer::Token *lexer::consumers::Consumer::process(std::ifstream &source) {
    std::string lexeme;

    auto current = initial;
    while (!source.eof() && source.peek() != -1) {
        auto next = delta(current, source.peek());
        if (next < 0) {
            break;
        }

        current = next;
        lexeme += static_cast<char>(source.get());
    }

    if (!isFinal(current)) {
        throw std::invalid_argument("invalid input");
    }

    return new Token(lexeme, getStateTokenType(current, lexeme));
}

void lexer::consumers::Consumer::raiseUndefinedTokenTypeForState(int state) {
    std::string error;
    error += std::to_string(state);
    throw std::invalid_argument(error);
}

lexer::consumers::Consumer::Consumer() {
    this->initial = 1;
}

lexer::consumers::Consumer::~Consumer() = default;
