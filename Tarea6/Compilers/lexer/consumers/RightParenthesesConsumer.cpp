//
// Created by Edwin Herrera on 11/16/17.
//

#include "RightParenthesesConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::RightParenthesesConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::rightParentheses) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::RightParenthesesConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::RightParenthesesConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::RIGHT_PAREN;
    }

    raiseUndefinedTokenTypeForState(state);
}
