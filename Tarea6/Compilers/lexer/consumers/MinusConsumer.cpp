//
// Created by Edwin Herrera on 11/16/17.
//

#include "MinusConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::MinusConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::minus) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumers::MinusConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumers::MinusConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::OP_SUB;
    }

    raiseUndefinedTokenTypeForState(state);
}
