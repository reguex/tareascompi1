//
// Created by Edwin Herrera on 11/16/17.
//

#include "SlashConsumer.h"
#include "../../util/Chars.h"
#include "../TokenType.h"

int lexer::consumers::SlashConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::slash) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::slash) {
            return 3;
        } else if (value == util::Chars::asterisk) {
            return 4;
        }
    } else if (current == 3) {
        if (static_cast<char>(value) != '\n' && value == -1) {
            return 3;
        }
    } else if (current == 4) {
        if (value != util::Chars::asterisk) {
            return 4;
        } else {
            return 5;
        }
    } else if (current == 5) {
        if (value == util::Chars::slash) {
            return 6;
        }
    }

    return -1;
}

bool lexer::consumers::SlashConsumer::isFinal(int state) {
    return state == 2 ||
           state == 3 ||
           state == 6;
}

std::string lexer::consumers::SlashConsumer::getStateTokenType(int state, const std::string &lexeme) {
    if (state == 2) {
        return lexer::TokenType::OP_DIV;
    } else if (state == 3 || state  == 6) {
        return lexer::TokenType::COMMENT;
    }

    raiseUndefinedTokenTypeForState(state);
}
