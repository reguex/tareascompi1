//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_LESSTHANCONSUMER_H
#define COMPILERS_LESSTHANCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class LessThanConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state, const std::string &lexeme) override;
        };
    }
}


#endif //COMPILERS_LESSTHANCONSUMER_H
