//
// Created by Edwin Herrera on 11/16/17.
//

#include "TokenType.h"


const std::string lexer::TokenType::ID = "ID";
const std::string lexer::TokenType::WHITESPACE = "WHITESPACE";
const std::string lexer::TokenType::COMMENT = "COMMENT";
const std::string lexer::TokenType::OP_DIV = "DIV";
const std::string lexer::TokenType::OP_MULT = "MULT";
const std::string lexer::TokenType::OP_PLUS = "PLUS";
const std::string lexer::TokenType::OP_SUB = "SUB";
const std::string lexer::TokenType::LEFT_PAREN = "LEFT PARENTHESES";
const std::string lexer::TokenType::RIGHT_PAREN = "RIGHT PARENTHESES";
const std::string lexer::TokenType::NUMBER = "NUMBER";
const std::string lexer::TokenType::EQUALS = "EQUALS";
const std::string lexer::TokenType::ASSIGNMENT = "ASSIGNMENT";
const std::string lexer::TokenType::KW_IF = "IF";
const std::string lexer::TokenType::KW_END = "END";
const std::string lexer::TokenType::KW_THEN = "THEN";
const std::string lexer::TokenType::SEMICOLON = "SEMICOLON";
const std::string lexer::TokenType::END_OF_FILE = "EOF";
const std::string lexer::TokenType::KW_PRINT = "PRINT";
const std::string lexer::TokenType::LESS_THAN = "LESS THAN";
const std::string lexer::TokenType::GREATER_THAN = "GREATER THAN";
const std::string lexer::TokenType::LESS_THAN_OR_EQUAL = "LESS THAN OR EQUAL";
const std::string lexer::TokenType::GREATER_THAN_OR_EQUAL = "GREATER THAN OR EQUAL";