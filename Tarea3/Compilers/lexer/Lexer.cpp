//
// Created by Edwin Herrera on 11/16/17.
//

#include "Lexer.h"
#include "TokenType.h"
#include "consumers/Consumers.h"

lexer::Lexer::Lexer(std::ifstream &input) : input(input) {}

lexer::Token *lexer::Lexer::getNextToken() {
    if (input.eof() || input.peek() == -1) {
        return nullptr;
    }

    consumers::Consumer *consumer = consumers::Consumers::getAppropriateConsumer(input.peek());
    Token *token = consumer->process(input);
    if (token->getTokenType() == lexer::TokenType::WHITESPACE || token->getTokenType() == lexer::TokenType::COMMENT) {
        delete token;
        delete consumer;
        return getNextToken();
    }

    return token;
}
