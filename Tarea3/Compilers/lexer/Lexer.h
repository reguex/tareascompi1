//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_LEXER_H
#define COMPILERS_LEXER_H

#include <fstream>
#include "consumers/Consumer.h"

#include "../util/Chars.h"

namespace lexer {
    class Lexer {
        std::ifstream &input;

    public:
        explicit Lexer(std::ifstream &input);

        Token *getNextToken();
    };
}


#endif //COMPILERS_LEXER_H
