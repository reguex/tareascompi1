//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_MINUSCONSUMER_H
#define COMPILERS_MINUSCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class MinusConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };

    }
}

#endif //COMPILERS_MINUSCONSUMER_H
