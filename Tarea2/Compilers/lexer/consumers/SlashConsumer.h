//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_SLASHCONSUMER_H
#define COMPILERS_SLASHCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class SlashConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_SLASHCONSUMER_H
