//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_IDCONSUMER_H
#define COMPILERS_IDCONSUMER_H


#include "Consumer.h"
#include "../../util/Chars.h"

namespace lexer {
    namespace consumers {
        class IdConsumer : public Consumer {
        public:
            IdConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_IDCONSUMER_H
