//
// Created by Edwin Herrera on 11/16/17.
//

#ifndef COMPILERS_NUMBERCONSUMER_H
#define COMPILERS_NUMBERCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumers {
        class NumberConsumer : public Consumer {
        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //COMPILERS_NUMBERCONSUMER_H
